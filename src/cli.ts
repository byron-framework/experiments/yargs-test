#!/usr/bin/env ts-node

import yargs from 'yargs';

yargs
  .commandDir('commands')
  .scriptName('byron')
  .demandCommand()
  .help()
  .argv;
