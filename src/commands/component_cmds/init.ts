import { Argv } from "yargs"

interface ARGV {
  [key: string]: unknown
}

const command = 'init [name]'

const desc = 'Initialize a componnent'

const builder = (yargs: Argv) => {
  return yargs.positional(
    'name',
    {
      describe: 'A name of the component'
    }
  )
}

const handler = (argv: ARGV) => {
  console.log('Starting ' + argv.name);
}

export {
  command,
  desc,
  builder,
  handler
}
