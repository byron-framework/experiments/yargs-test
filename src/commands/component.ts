import { Argv } from "yargs";

interface ARGV {
  [key: string]: unknown
}

const command: string = 'component <command>'

const desc: string = 'Manage components'

const builder = (yargs: Argv) => {
  return yargs.commandDir('component_cmds')
}

const handler = (argv: ARGV) => {}

export {
  command,
  desc,
  builder,
  handler
}